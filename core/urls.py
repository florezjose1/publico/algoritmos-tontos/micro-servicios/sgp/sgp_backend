from django.urls import path, include
from rest_framework import routers

from core import views

router = routers.DefaultRouter()
router.register(r'categories', views.CategoryViewSet)

app_name = 'core'
urlpatterns = [
    path('', include(router.urls)),
]
