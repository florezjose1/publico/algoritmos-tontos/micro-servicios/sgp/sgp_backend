from django.db.models import Q, Count, Prefetch

from authentication.models import UserInformation
from projects.controller import ControllerBase
from projects.models import Project, Score
from projects.serializers import ProjectSerializer


class WriterController(ControllerBase):

    STATUS_UPDATED_WRITER = [Project.FOR_CORRECTION, Project.DRAFT, Project.CREATED]
    STATUS_UPDATED_WRITER_NEXT = [Project.SEND]

    @staticmethod
    def get_profile(user_id, request):
        info = UserInformation.objects.get(user_id=user_id)
        return {
            "username": info.user.username,
            "role": info.role,
        }

    @staticmethod
    def serializer_projects(projects):
        data = list()
        for pro in projects:
            data.append({
                'id': pro.id,
                'title': pro.title,
                'description': pro.description,
                'status': pro.status,
            })
        return {
            "data": data
        }

    @classmethod
    def get_draft_projects(cls, user_id, request):
        projects = Project.objects.filter(
            status__in=cls.STATUS_UPDATED_WRITER,
            author_id=user_id
        )
        return cls.serializer_projects(projects)

    @classmethod
    def get_qualified_projects(cls, user_id, request):
        projects = Project.objects.filter(
            author_id=user_id,
            status=Project.QUALIFIED
        )
        return cls.serializer_projects(projects)

    @classmethod
    def get_review_projects(cls, user_id, request):
        projects = Project.objects.filter(
            author_id=user_id,
            status=Project.SEND,
            projects_score__evaluator_id__isnull=False
        )
        return cls.serializer_projects(projects)

    @classmethod
    def get_not_qualifier_projects(cls, user_id, request):
        data = Project.objects.filter(
            author_id=user_id
        ).exclude(
            Q(status=Project.FOR_CORRECTION) |
            Q(status=Project.DRAFT) |
            Q(status=Project.CREATED)
        ).exclude(
            projects_score__evaluator_id__isnull=False
        )
        return cls.serializer_projects(data)

    @classmethod
    def get_view_projects(cls, user_id, request):
        _id = request.query_params.get('id')
        serializer_data = cls.base_view_projects({"id": _id, "author_id": user_id})
        return serializer_data

    @classmethod
    def get_count_projects(cls, user_id, request):
        return cls.base_count_projects(user_id)

    @staticmethod
    def update_categories(project: Project, categories: list, is_updated: bool):
        """"""
        categories_old = list(project.category.all().only('id').values_list('id', flat=True))
        for c in categories_old:
            if not c in categories:
                project.category.remove(c)
                is_updated = True
            else:
                categories.remove(c)

        for c in categories:
            project.category.add(c)
            is_updated = True

        return is_updated

    @classmethod
    def update_project(cls, user_id, id_project: int, data: dict):
        """"""
        project = Project.objects.get(
            author_id=user_id,
            id=id_project,
            status__in=cls.STATUS_UPDATED_WRITER
        )
        if not project:
            message = 'Actualización no permitida'
            alert = 'error'
            return message, alert, id_project

        status = data.get('status')
        if status != project.status and not status in cls.STATUS_UPDATED_WRITER_NEXT:
            message = 'Estado de actualización no permitido'
            alert = 'error'
            return message, alert, id_project

        title = data.get('title')
        description = data.get('description')
        is_updated = False
        if project.title != title:
            project.title = title
            is_updated = True
        if project.status != status:
            project.status = status
            is_updated = True
        if project.description != description:
            project.description = description
            is_updated = True

        ids_categories = list()
        for id in data['categories']:
            ids_categories.append(id.get("id"))
        is_updated = cls.update_categories(project, ids_categories, is_updated)

        if is_updated:
            project.save()
            message = 'Proyecto actualizado'
            alert = 'success'
        else:
            message = 'No tenemos información para actualizar'
            alert = 'warning'

        return message, alert, project.id

    @staticmethod
    def get_evaluators(*args):
        return []
