from django.urls import path, include

from projects import views

app_name = 'projects'
urlpatterns = [
    path('<str:action>/', views.ProjectView.as_view(), name='get_file'),
    path('file/<int:id>', views.GetFileView.as_view(), name='get_file'),
    path('upload-file/', views.UploadFileView.as_view(), name='upload_files'),
]
