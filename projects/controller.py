import abc

from django.db.models import Prefetch, Count

from projects.models import Project, Score


class ControllerBase(abc.ABC):
    STATUS_UPDATED = [Project.FOR_CORRECTION, Project.DRAFT, Project.CREATED]

    @classmethod
    def base_view_projects(cls, filters):
        project = Project.objects.prefetch_related(
            Prefetch('score_projects', queryset=Score.objects.select_related('evaluator__user'))
        ).get(**filters)
        serializer_data = cls.serializer_project(project)
        scores = project.score_projects.all()
        data_scores = []
        for score in scores:
            data_scores.append({
                "id": score.evaluator_id,
                "name": score.evaluator.user.username,
                "score": int(score.score),
                "comment": score.comment,
            })
        serializer_data["score"] = data_scores
        return serializer_data

    @staticmethod
    def get_projects(filters):
        return Project.objects.filter(
            **filters
        )

    @classmethod
    def base_count_projects(cls, user_id: int = None):
        filters = {"status": ""}
        if user_id:
            filters["author_id"] = user_id

        filters.update({"status": Project.QUALIFIED})
        qualified = cls.get_projects(filters).count()

        del filters['status']
        filters.update({"status__in": cls.STATUS_UPDATED})
        for_correction = cls.get_projects(filters).count()
        del filters['status__in']

        filters.update({"status": Project.SEND})
        total_not_qualifiers = cls.get_projects(
            filters
        ).annotate(num_scores=Count('projects_score__id')).filter(num_scores=0).count()

        filters.update({"status": Project.SEND, "projects_score__evaluator_id__isnull": False})
        send = cls.get_projects(
            filters
        ).distinct().count()

        return {
            'qualified': qualified,
            'send': send,
            'not_qualifier': total_not_qualifiers,
            'draft': for_correction,
        }

    @staticmethod
    def serializer_project(project):
        return {
            'id': project.id,
            'title': project.title,
            'description': project.description,
            'status': project.status,
            'categories': [{'id': cat.id, 'name': cat.name} for cat in project.category.only('id', 'name')],
        }

    @staticmethod
    def serializer_projects(projects):
        data = [
            {
                'id': pro.id,
                'title': pro.title,
                'description': pro.description,
                'status': pro.status,
            }
            for pro in projects
        ]
        return {
            "data": data
        }

    @classmethod
    @abc.abstractmethod
    def get_draft_projects(cls, user_id, request):
        pass

    @classmethod
    @abc.abstractmethod
    def get_draft_projects(cls, user_id, request):
        pass

    @classmethod
    @abc.abstractmethod
    def get_qualified_projects(cls, user_id, request):
        pass

    @classmethod
    @abc.abstractmethod
    def get_review_projects(cls, user_id, request):
        pass

    @classmethod
    @abc.abstractmethod
    def get_not_qualifier_projects(cls, user_id, request):
        pass

    @classmethod
    @abc.abstractmethod
    def get_view_projects(cls, user_id, request):
        pass

    @staticmethod
    @abc.abstractmethod
    def get_count_projects(user_id, request):
        pass

    @classmethod
    @abc.abstractmethod
    def update_project(cls, user_id, id_project: int, data: dict):
        pass
