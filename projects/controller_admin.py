from django.db.models import Q, Count, Prefetch

from evaluator.models import Evaluator
from projects.controller import ControllerBase
from projects.models import Project, Score
from projects.serializers import ProjectSerializer


class AdminController(ControllerBase):

    @classmethod
    def get_draft_projects(cls, user_id, request):
        projects = cls.get_projects({"status__in": cls.STATUS_UPDATED})
        return cls.serializer_projects(projects)

    @classmethod
    def get_qualified_projects(cls, user_id, request):
        projects = cls.get_projects({"status": Project.QUALIFIED})
        return cls.serializer_projects(projects)

    @classmethod
    def get_review_projects(cls, user_id, request):
        projects = cls.get_projects({"status": Project.SEND, "projects_score__evaluator_id__isnull": False}).distinct()
        return cls.serializer_projects(projects)

    @classmethod
    def get_not_qualifier_projects(cls, user_id, request):
        data = Project.objects.all().exclude(
            status__in=cls.STATUS_UPDATED
        ).exclude(
            projects_score__evaluator_id__isnull=False
        )
        return cls.serializer_projects(data)

    @classmethod
    def get_view_projects(cls, user_id, request):
        _id = request.query_params.get('id')
        serializer_data = cls.base_view_projects({"id": _id})
        return serializer_data

    @classmethod
    def get_count_projects(cls, user_id, request):
        return cls.base_count_projects()

    @classmethod
    def update_project(cls, user_id, id_project: int, data: dict):
        """"""
        project = Project.objects.get(
            id=id_project
        )
        if not project:
            message = 'Actualización no permitida'
            alert = 'error'
            return message, alert, id_project

        status = data.get('status')
        title = data.get('title')
        description = data.get('description')
        is_updated = False
        if project.title != title:
            project.title = title
            is_updated = True
        if project.status != status:
            project.status = status
            is_updated = True
        if project.description != description:
            project.description = description
            is_updated = True

        evaluators = data.get("evaluators")
        errors = list()
        for ev in evaluators:
            _id = ev.get("id")
            _name = ev.get("name")
            _score = ev.get("score", 0)
            evaluator = Evaluator.objects.filter(user_id=_id).first()
            if not evaluator:
                errors.append({
                    "message": f"Evaluador: {_name} no existe"
                })
            else:
                score = Score.objects.filter(evaluator__user_id=_id, project_id=project.id)
                if not score:
                    score = Score.objects.create(
                        evaluator_id=evaluator.id,
                        project_id=project.id,
                        score=_score
                    )

        if is_updated:
            project.save()
            message = 'Proyecto actualizado'
            alert = 'success'
        else:
            message = 'No tenemos información para actualizar'
            alert = 'warning'

        return message, alert, project.id

    @staticmethod
    def get_evaluators(*args):
        evaluators = [
            {
                "id": ev.user_id,
                "name": ev.user.username,
            }
            for ev in Evaluator.objects.select_related('user').all()
        ]
        return evaluators
