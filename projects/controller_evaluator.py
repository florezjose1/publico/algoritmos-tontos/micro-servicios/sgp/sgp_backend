from django.db.models import Sum, Prefetch

from projects.models import FilesApp, Score, Project


class EvaluatorController:

    STATUS_TO_QUALIFIED = [Project.FOR_CORRECTION, Project.IN_CORRECTION, Project.SEND]

    @staticmethod
    def serializer(projects):
        data = list()
        for pro in projects:
            data.append({
                'id': pro.id,
                'title': pro.title,
                'description': pro.description,
                'status': pro.status,
            })
        return {
            "data": data
        }

    @classmethod
    def get_count_projects(cls, user_id, request):
        to_qualified = Project.objects.filter(
            status__in=cls.STATUS_TO_QUALIFIED,
            projects_score__evaluator__user_id=user_id
        ).distinct().count()

        qualified = Project.objects.filter(
            status=Project.QUALIFIED,
            projects_score__evaluator__user_id=user_id
        ).distinct().count()

        return {
            'send': to_qualified,
            'qualified': qualified,
        }

    @classmethod
    def get_review_projects(cls, user_id, request):
        projects = Project.objects.filter(
            projects_score__evaluator__user_id=user_id,
            status=Project.SEND,
        )
        return cls.serializer(projects)

    @classmethod
    def get_view_projects(cls, user_id, request):
        _id = request.query_params.get('id')
        project = Project.objects.prefetch_related(
            Prefetch('score_projects', queryset=Score.objects.select_related('evaluator__user'))
        ).get(id=_id, projects_score__evaluator__user_id=user_id)
        serializer_data = {
            'id': project.id,
            'title': project.title,
            'description': project.description,
            'status': project.status,
        }

        score = project.score_projects.filter(evaluator__user_id=user_id).first()
        data_score = {
            "id": score.evaluator_id,
            "name": score.evaluator.user.username,
            "score": score.score,
            "comment": score.comment,
        }

        serializer_data["score"] = data_score
        return serializer_data

    @classmethod
    def get_qualified_projects(cls, user_id, request):
        projects = Project.objects.filter(
            projects_score__evaluator__user_id=user_id,
            status=Project.QUALIFIED
        )
        return cls.serializer(projects)

    @staticmethod
    def get_evaluators(*args):
        return []

    @classmethod
    def update_project(cls, user_id, id_project: int, data: dict):
        """"""
        project = Project.objects.prefetch_related(
            Prefetch('score_projects', queryset=Score.objects.select_related('evaluator__user'))
        ).get(
            id=id_project,
            status=Project.SEND,
            projects_score__evaluator__user_id=user_id
        )

        if not project:
            message = 'Actualización no permitida'
            alert = 'error'
            return message, alert, id_project

        score = project.score_projects.filter(
            evaluator__user_id=user_id
        ).first()
        is_updated = False
        if data.get("score").get("score") > 0:
            score.score = data.get("score").get("score")
            is_updated = True
        if data.get("score").get("comment"):
            score.comment = data.get("score").get("comment")
            is_updated = True
        if is_updated:
            score.qualified = True
            score.save()

        scores = Score.objects.filter(project_id=project.id, qualified=False).count()
        if scores == 0:
            project.status = Project.QUALIFIED
            project.save()

        return "", "", project.id

    @staticmethod
    def get_score_project(project_id: int, request=None):
        if request:
            scores = Score.objects.filter(
                project_id=project_id,
                evaluator__user_id=request.user.id
            ).only('score', 'extra')
        else:
            scores = Score.objects.filter(
                project_id=project_id
            ).only('score', 'extra')

        score_g = scores.first()
        score_g = int(score_g.score) if score_g else 0

        return {
            'score_g': score_g,
        }
