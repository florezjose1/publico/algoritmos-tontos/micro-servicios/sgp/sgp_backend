import json

from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponse, JsonResponse
from django.shortcuts import render
from django.utils.decorators import method_decorator
from django.views import View
from django.views.decorators.csrf import csrf_exempt, csrf_protect
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView

from authentication.models import UserInformation
from core.models import Category
from projects.controller_evaluator import EvaluatorController
from projects.controller_admin import AdminController
from projects.controller_writer import WriterController
from projects.models import FilesApp, Project
from projects.serializers import ProjectSerializer


class ProjectView(APIView):
    permission_classes = [IsAuthenticated]

    resolvers = {
        UserInformation.WRITER: WriterController,
        UserInformation.EVALUATOR: EvaluatorController,
        UserInformation.ADMIN: AdminController,
    }

    @staticmethod
    def get_role(user_id):
        information = UserInformation.objects.get(user_id=user_id)
        return information.role

    def resolve_user(self, user_id):
        role = self.get_role(user_id)
        return self.resolvers.get(role)

    def get(self, request, action, *args, **kwargs):
        user_id = request.user.id
        resolver = self.resolve_user(user_id)
        action_methods = {
            "count": resolver.get_count_projects,
            "draft": resolver.get_draft_projects,
            "qualified": resolver.get_qualified_projects,
            "review": resolver.get_review_projects,
            "not-qualifier": resolver.get_not_qualifier_projects,
            "view": resolver.get_view_projects,
            "evaluators": resolver.get_evaluators,
        }
        data = action_methods.get(action)(user_id, request)
        return JsonResponse(data, safe=False)

    @staticmethod
    def resolve_status(data):
        status = data.get("status")
        if status and status in Project.STATUS_CREATE_LIST:
            return status

        return Project.CREATED

    def post(self, request, *args, **kwargs):
        data = json.loads(request.body)
        project = Project.objects.create(
            author=request.user,
            title=data['title'],
            description=data['description'],
            status=self.resolve_status(data),
            created_by=request.user.id
        )

        ids_categories = list()
        for id in data['categories']:
            ids_categories.append(id.get("id"))

        categories = Category.objects.filter(id__in=ids_categories).only('id').values_list('id', flat=True)

        for c in categories:
            project.category.add(c)

        return JsonResponse(
            status=200,
            data={
                'id': project.id,
                'message': 'Proyecto creado exitosamente',
                'alert': 'success'
            }
        )

    def put(self, request, *args, **kwargs):
        user_id = request.user.id
        resolver = self.resolve_user(user_id)

        data = json.loads(request.body)
        user_id = request.user.id
        id_project = data.get('id', None)
        if not id_project:
            return JsonResponse(status=400, data={'message': 'Id proyecto no identificado', 'alert': 'warning'})

        message, alert, project_id = resolver.update_project(user_id, id_project, data)

        return JsonResponse(
            status=200,
            data={
                'message': message,
                'alert': alert,
                'id': project_id
            }
        )


class GetFileView(View):
    def get(self, request, *args, **kwargs):
        files = FilesApp.objects.filter(
            project_id=kwargs.get('id')
        )
        data = [{'id': f.id, 'name': f.name, 'type_file': f.type_file, 'file': f"/media/{f.file}"} for f in files]
        return JsonResponse(status=200, data={'files': data})

    def delete(self, request, *args, **kwargs):
        data = json.loads(request.body)
        query = FilesApp.objects.get(
            id=data.get('id'),
            project__author_id=request.user.id
        )
        query.delete()
        return JsonResponse(status=200, data={'id': query.id, 'message': 'Archivo eliminado'})


@method_decorator(csrf_exempt, name='dispatch')
class UploadFileView(View):
    permission_classes = [IsAuthenticated]

    @staticmethod
    def extension_file(filename):
        ext = filename.split('.')[-1]
        allowed_extensions = [FilesApp.PDF, FilesApp.PNG, FilesApp.JPG, FilesApp.JPEG]
        if not ext in allowed_extensions:
            raise "Extension no válida"

        return ext

    @method_decorator(csrf_protect)
    def post(self, request, *args, **kwargs):
        try:
            id_project = request.POST.get("id_project", None)
            if not id_project:
                raise "Id proyecto no identificado"

            project = Project.objects.get(id=id_project)
            name = request.FILES['file'].name
            FilesApp.objects.create(
                name=name,
                file=request.FILES['file'],
                type_file=self.extension_file(name),
                project=project
            )
            return HttpResponse(status=200)
        except Exception as e:
            return JsonResponse(status=400, data={'message': f"{e}"})
