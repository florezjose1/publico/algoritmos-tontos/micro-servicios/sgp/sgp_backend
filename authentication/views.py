import json

from django.contrib import messages
from django.contrib.auth import authenticate, login
from django.contrib.auth.models import User
from django.db.models import Q
from django.http import JsonResponse
from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views import View
from django.views.decorators.csrf import csrf_protect, csrf_exempt
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView

from authentication.models import UserInformation
from evaluator.models import Evaluator


def redirect_session(request):
    if request.user.is_authenticated:
        user = UserInformation.objects.get(user_id=request.user.id)
        if user.role == UserInformation.ADMIN:
            return redirect(reverse_lazy('administrator:dashboard'))
        elif user.role == UserInformation.EVALUATOR:
            return redirect(reverse_lazy('evaluator:dashboard'))
        elif user.role == UserInformation.WRITER:
            return redirect(reverse_lazy('writer:projects'))
        else:
            messages.warning(request, 'Usuario sin rol para el sistema')
            return redirect(reverse_lazy('authentication:logout'))

    return redirect(reverse_lazy('authentication:login'))


class SignInView(View):
    template_name = "authentication/login.html"

    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            return redirect(reverse_lazy('authentication:redirect_session'))
        return render(request, self.template_name)

    def post(self, request, *args, **kwargs):
        username = request.POST.get('username', None)
        password = request.POST.get('password', None)
        if User.objects.filter(username__exact=username).exists():
            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)
                return redirect(reverse_lazy('authentication:redirect_session'))

        messages.warning(request, 'Usuario o contraseña incorrecto. Por favor intentalo de nuevo')
        return redirect(reverse_lazy('authentication:login'))


@method_decorator(csrf_exempt, name='dispatch')
class SignUpView(View):
    template_name = "authentication/register.html"

    def get(self, request, *args, **kwargs):
        return render(request, self.template_name)

    @csrf_exempt
    def post(self, request, *args, **kwargs):
        json_data = json.loads(request.body)
        username = json_data.get('username', None)
        email = json_data.get('email', None)
        role = json_data.get('role', None)
        user_exist = User.objects.get(username__exact=username, email=email)
        message = None

        if not user_exist:
            message = "Este usuario no se encuentra registrado."

        if not message:
            user_information = UserInformation(
                user=user_exist,
                role=role
            )
            user_information.save()

            if user_information.role == UserInformation.EVALUATOR:
                Evaluator.objects.create(user=user_exist)

            return JsonResponse(status=200, data={'message': f"Usuario registrado exitosamente"})
        else:
            messages.error(request, message)

        return redirect(reverse_lazy('authentication:register'))


class ProfileView(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request, *args, **kwargs):
        user_id = request.user.id
        info = UserInformation.objects.get(user_id=user_id)
        return JsonResponse({
            "username": info.user.username,
            "role": info.role,
        }, safe=False)
